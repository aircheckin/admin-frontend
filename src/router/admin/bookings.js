import Bookings from '@/views/bookings/BookingsList'

export default {
	path: '/bookings',
	component: {
		render: (c) => c('router-view'),
	},
	children: [
		{
			name: 'bookings',
			path: '/',
			component: Bookings,
		},
	],
}
