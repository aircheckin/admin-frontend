import Apartments from '@/views/Apartments'

export default {
	name: 'apartments',
	path: '/apartments',
	component: Apartments,
}
