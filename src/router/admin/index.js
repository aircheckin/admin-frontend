import Dashboard from '@/views/Dashboard'

import dashboard from '@/router/admin/dashboard'
import users from '@/router/admin/users'
import owners from '@/router/admin/owners'
import apartments from '@/router/admin/apartments'
import owners_contracts from '@/router/admin/owners_contracts'
import bookings from '@/router/admin/bookings'
import _template from '@/router/admin/_template'
import renters from '@/router/admin/renters'

export default {
	path: '/',
	meta: {
		accessRoles: ['administrator', 'main administrator'],
	},
	component: {
		render: (c) => c('router-view'),
	},
	children: [
		dashboard,
		users,
		owners,
		apartments,
		owners_contracts,
		bookings,
		renters,
		_template,
		{ path: '*', component: Dashboard },
	],
}
