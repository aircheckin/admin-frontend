import OwnersContracts from '@/views/OwnersContracts'

export default {
	name: 'owners_contracts',
	path: '/owners-contracts',
	component: OwnersContracts,
}
