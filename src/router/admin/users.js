import Users from '@/views/Users'

export default {
	name: 'users',
	path: '/users',
	component: Users,
	meta: {
		title: 'Пользователи',
	},
}
