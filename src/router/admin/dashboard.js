import Dashboard from '@/views/Dashboard'

export default {
	name: 'dashboard',
	path: '/',
	component: Dashboard,
	meta: {
		title: 'Рабочий стол',
	},
}
