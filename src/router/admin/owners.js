import Owners from '@/views/Owners'

export default {
	name: 'owners',
	path: '/owners',
	component: Owners,
}
