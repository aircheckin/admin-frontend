import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '@/router/auth/login'
import registration from '@/router/auth/registration'
import verification from '@/router/auth/verification'
import admin from '@/router/admin'
import store from '@/store'
import i18n from '@/i18n'
import { hasRole } from '@/utils/permissions'

Vue.use(VueRouter)

const routes = [
	login,
	registration,
	verification,
	admin,
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
})

const getInitData = () => new Promise((resolve, reject) => {
	if (store.state.auth.loggedIn) {
		resolve()
	} else {
		store.dispatch('global/setLoading', true)
		store.dispatch('auth/getInitData')
			.then(() => resolve())
			.catch(() => reject())
			.finally(() => store.dispatch('global/setLoading', false))
	}
})

router.beforeEach((to, from, next) => {
	let accessRoles = []

	to.matched.some(record => {
		if (record.meta.accessRoles) {
			accessRoles = record.meta.accessRoles
		}
	})

	if (accessRoles.length === 0) {
		next()
	} else if (store.state.auth.token) {
		getInitData().then(() => {
			if (store.state.auth.verified) {

				if (hasRole(accessRoles)) {
					next()
				} else {
					Vue.prototype.$snotify.error(i18n.tc('accessDenied'))
					throw false
				}
			} else {
				next({ name: 'verification', query: { email: store.state.auth.user.email } })
			}
		}).catch(() => {
			store.dispatch('auth/logout')
		})

	} else {
		next('login')
	}
})

export default router
