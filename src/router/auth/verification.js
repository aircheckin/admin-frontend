import Verification from '@/views/auth/Verification'
import regex from '@/utils/regex'
import store from '@/store'

export default {
	name: 'verification',
	path: '/verification',
	component: Verification,
	meta: { layout: 'AuthLayout' },

	beforeEnter: ({ query: { email } }, _, next) => {
		if (email && regex.email.test(email) && !store.state.auth.verified) {
			next()
		} else {
			next('/')
		}
	},
}
