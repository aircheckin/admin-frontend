import Login from '@/views/auth/Login'
import store from '@/store'

export default {
	name: 'login',
	path: '/login',
	alias: '/reset-password',
	component: Login,
	meta: { layout: 'AuthLayout' },
	beforeEnter (to, from, next) {
		if (store.state.auth.token && store.state.auth.verified) {
			next('/dashboard')
		} else {
			next()
		}
	},
}
