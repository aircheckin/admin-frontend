import Registration from '@/views/auth/Registration'
import store from '@/store'

export default {
	name: 'registration',
	path: '/registration/:userIdHash?',
	component: Registration,
	meta: { layout: 'AuthLayout' },
	beforeEnter (to, from, next) {
		const loggedIn = store.state.auth.token && store.state.auth.verified

		if (loggedIn) {
			next('/')
		} else {
			next()
		}
	},
}
