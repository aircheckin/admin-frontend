import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

import './plugins/notifications'
import './plugins/yandexMap'
import './plugins/uploader'
import './plugins/copy'

import './styles/styles.scss'

import i18n from './i18n'

Vue.config.productionTip = false

const moment = require('moment')
require('moment/locale/ru')
moment.suppressDeprecationWarnings = true

Vue.use(require('vue-moment'), {
	moment,
})


Vue.use(moment)

new Vue({
	router,
	store,
	vuetify,
	i18n,
	render: h => h(App),
}).$mount('#app')
