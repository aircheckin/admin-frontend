import Vue from 'vue'
import VueFileAgent from 'vue-file-agent'

import { SlickItem, SlickList } from 'vue-slicksort'

Vue.component('vfa-sortable-list', SlickList)
Vue.component('vfa-sortable-item', SlickItem)

Vue.use(VueFileAgent)
