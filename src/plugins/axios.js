import axios from 'axios'
import i18n from '@/i18n'
import Vue from 'vue'

// Add a request interceptor
axios.interceptors.request.use(function (config) {
	const token = localStorage.getItem('token')

	if (token) {
		config.headers.common['Authorization'] = 'Bearer ' + token
	}

	config.baseURL = process.env.VUE_APP_API_URL
	config.withCredentials = true

	return config
})

axios.interceptors.response.use(function (response) {
	// Всё в порядке, статус ответа без ошибок
	return response
}, function (error) {
	handleErrors(error.response)

	return Promise.reject(error)
})

const handleErrors = (response => {
	const hasExceptionWithoutMessage = response.data && response.data.exception && !response.data.message
	let errorMessage = ''

	if (!response) {
		errorMessage = i18n.tc('serviceError')
	} else if (response.status === 404) {
		errorMessage = i18n.tc('notFound')
	} else if (response.status === 400 || response.status === 401 || response.status === 403) {
		errorMessage = i18n.tc('accessDenied')
	} else if (response.status === 500 || hasExceptionWithoutMessage) {
		errorMessage = i18n.tc('serviceError')
	} else if (response.data) {
		const { errors, message } = response.data

		if (message && errors) {
			let errorFields = []

			Object.entries(errors).forEach((value) => {
				if (value[0]) {
					errorFields.push(i18n.tc('fields.' + value[0]))
				}
			})

			errorMessage = 'Ошибка в данных: ' + errorFields.join(', ')
		} else if (message) {
			errorMessage = message
		}
	}

	Vue.prototype.$snotify.error(errorMessage)
})

export default axios
