import Vue from 'vue'
import snotify, { SnotifyPosition } from 'vue-snotify'

const options = {
	toast: {
		position: SnotifyPosition.rightTop,
		showProgressBar: false,
		timeout: 2500,
		animation: { enter: 'fadeIn', exit: 'fadeOut', time: 600 },
	},
}

Vue.use(snotify, options)
