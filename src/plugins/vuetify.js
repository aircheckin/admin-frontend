import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'
import ru from 'vuetify/lib/locale/ru'
import {
	mdiAccount,
	mdiAccountMultiple,
	mdiAlarm,
	mdiBook,
	mdiCalendar,
	mdiCheck,
	mdiDelete,
	mdiDotsVertical,
	mdiDownload,
	mdiEye,
	mdiEyeOff,
	mdiFileDocument,
	mdiHome,
	mdiInformationOutline,
	mdiLogout,
	mdiMessage,
} from '@mdi/js'
import VuetifyConfirm from '@/plugins/confirm/index.js'

import i18n from '@/i18n'

Vue.use(Vuetify)

const VuetifyInstance = new Vuetify({
	theme: {
		options: {
			customProperties: true,
		},
		themes: {
			primaryTheme: 'light',
			secondaryTheme: 'light',
			light: {
				primary: '#34495e',
				secondary: '#41b883',
				accent: '#82B1FF',
				error: '#FF5252',
				info: '#2196F3',
				success: '#4CAF50',
				warning: '#FFC107',
			},
		},
	},
	lang: {
		locales: { ru },
		current: 'ru',
	},
	icons: {
		iconfont: 'mdiSvg',
		values: {
			eye: mdiEye,
			eyeOff: mdiEyeOff,
			trash: mdiDelete,
			home: mdiHome,
			calendar: mdiCalendar,
			document: mdiFileDocument,
			book: mdiBook,
			account: mdiAccount,
			accounts: mdiAccountMultiple,
			logout: mdiLogout,
			info: mdiInformationOutline,
			check: mdiCheck,
			time: mdiAlarm,
			verticalDots: mdiDotsVertical,
			download: mdiDownload,
			message: mdiMessage,
		},

		/*
		cancel
		checkboxIndeterminate
		checkboxOff
		checkboxOn
		clear
		close
		complete
		delete
		delimiter
		dropdown
		edit
		error
		expand
		file
		first
		info
		last
		loading
		menu
		minus
		next
		plus
		prev
		radioOff
		radioOn
		ratingEmpty
		ratingFull
		ratingHalf
		sort
		subgroup
		success
		unfold
		warning
		*/
	},
})

Vue.use(VuetifyConfirm, {
	vuetify: VuetifyInstance,
	buttonTrueText: i18n.tc('confirm'),
	buttonFalseText: i18n.tc('cancel'),
	buttonTrueColor: 'primary',
	buttonFalseColor: 'grey',
	buttonTrueFlat: false,
	buttonFalseFlat: true,
	width: 350,
	color: 'primary',
	property: '$confirm',
})

export default VuetifyInstance
