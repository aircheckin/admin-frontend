export const buildName = (lastName = null, firstName = null, middleName = null) => {
	let name = ''

	if (lastName) {
		name += lastName + ' '
	}

	if (firstName) {
		name += firstName + ' '
	}

	if (middleName) {
		name += middleName
	}

	name = name.trim()
	return name.length > 0 ? name : ''
}
