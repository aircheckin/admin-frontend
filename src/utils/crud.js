import axios from '../plugins/axios'
import Vue from 'vue'
import i18n from '@/i18n'

export const getItem = ({ commit }, url, id) => {
	commit(`SET_ERROR`, null)
	commit(`SET_ITEM_LOADING`, true)

	return axios
		.get(`${url}/${id}`)
		.then(response => response.data)
		.then(({ data }) => {
			commit(`SET_ITEM`, data)

			return data
		})
		.catch(e => {
			commit(`SET_ERROR`, e.message)
		})
		.finally(() => {
			commit(`SET_ITEM_LOADING`, false)
		})
}

export const getItems = ({ commit }, url, query) => {
	if (query) {
		url += `?${Object.keys(query)
			.map(key => `${key}=${query[key]}`)
			.join('&')}`
	}

	commit(`SET_ERROR`, null)
	commit(`SET_ITEMS`, [])
	commit(`SET_LOADING`, true)

	return axios
		.get(process.env.VUE_APP_API_URL + url)
		.then(response => response.data)
		.then(({ data }) => {
			commit(`SET_ITEMS`, data)

			return data
		})
		.catch(e => {
			commit(`SET_ERROR`, e.message)
		})
		.finally(() => {
			commit(`SET_LOADING`, false)
		})

}

export const create = ({ commit, state }, url) => {

	commit(`SET_ERRORS`, {})

	return axios
		.post(url, state.item)
		.catch(e => {
			throw e.response
		})
}

export const update = ({ commit, state }, url) => {
	commit(`SET_ERROR`, null)

	return axios
		.put(`${url}/${state.item.id}`, state.item)
		.then(() => Vue.prototype.$snotify.success(i18n.tc('saved')))
		.then(response => response.data)
		.catch(e => {
			throw e.response
		})
}

export const remove = (_, url, item) =>
	axios
		.delete(`${url}/${item.id}`)
		.then(({ data: { message } }) => {
			Vue.prototype.$snotify.success(message)
		})
		.catch(e => {
			throw e
		})

export const reset = ({ commit }) => {
	commit(`RESET`)
}
