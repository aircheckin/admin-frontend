export const BOOKING_CREATED = 1
export const BOOKING_BOOKED = 2
export const BOOKING_CONFIRMED = 3
export const BOOKING_REGISTERED = 4
export const BOOKING_PAID = 5
export const BOOKING_COMPLETED = 8
export const BOOKING_CANCELLED = 9
