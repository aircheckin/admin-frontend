import store from '@/store'

export const hasRole = (roles) => {
	return store.getters['auth/hasRole'](roles)
}

export const hasPermission = (permission) => {
	return store.getters['auth/hasPermission'](permission)
}
