export const formatPhone = (phone) => {
	return phone.replace(/[^+\d]/g, '')
}
