import regex from '@/utils/regex'
import i18n from '@/i18n'

export const nameRules = () => {
	return [
		v => !!v || i18n.tc('auth.nameRequired'),
		v => regex.name.test(v) || i18n.tc('auth.incorrectName'),
	]
}

export const emailRules = () => {
	return [
		v => !!v || i18n.tc('auth.emailRequired'),
		v => regex.email.test(v) || i18n.tc('auth.incorrectEmail'),
	]
}

export const phoneRules = () => {
	return [
		v => !!v || i18n.tc('auth.phoneRequired'),
		v => regex.phone.test(v) || i18n.tc('auth.incorrectPhone'),
	]
}

export const passwordRules = () => {
	return [
		v => !!v || i18n.tc('auth.passwordRequired'),
		v => regex.password.test(v) || i18n.tc('auth.incorrectPassword'),
		v => v.length >= 8 || i18n.tc('auth.passwordMinLength'),
	]
}
