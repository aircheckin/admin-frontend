export const ownerProfit = (periodPrice, serviceIncome, cleaningsCost) => {
	let profit = 0

	if (periodPrice) {
		profit += periodPrice - serviceIncome - cleaningsCost
	}

	return profit
}

export const serviceIncome = (periodPrice, servicePercent) => {
	let income = 0

	if (periodPrice && servicePercent) {
		income = periodPrice * servicePercent / 100
	}

	return income
}

export const cleaningsCost = (count, price) => {
	let cost = 0

	if (count && price) {
		cost = count * price
	}

	return cost
}
