export default {
	invite: 'Пригласить администратора',
	invitation: 'Приглашение',
	sendInvite: 'Отправить',
	inviteSent: 'Приглашение отправлено',
}
