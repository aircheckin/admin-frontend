export default `1. <a href="#1">Основные понятия и определения</a>

  2. <a href="#2">Бронирование и отмена бронирования</a>

  3. <a href="#3">Депозит</a>

  4. <a href="#4">Тарифы и прейскурант дополнительных услуг</a>

  5. <a href="#5">Осмотр Апартамента и получение ключей Арендатором</a>

  6. <a href="#6">Порядок заселения/выселения</a>

  7. <a href="#7">Пользование Апартаментом</a>

  8. <a href="#8">Пользование Местами общего пользования</a>

  9. <a href="#9">Запрет курения</a>

  10. <a href="#10">Соблюдение правил пожарной безопасности</a>

  11. <a href="#11">Права, обязанности и ответственность Арендодателя</a>

  12. <a href="#12">Права, обязанности и ответственность Арендатора</a>

  13. <a href="#13">Ответственность за несоблюдение Правил</a>

  14. <a href="#14">Досрочное выселение</a>

  15. <a href="#15">Реквизиты</a>
  
  
  <strong><span id="1">1. Основные понятия и определения</span></strong>

  Администратор – представитель Арендодателя, являющийся контактным лицом для Арендатора и Жильцов.

  Анкета-договор – договор аренды в отношении Апартамента и принадлежностей, заключаемый между Арендодателем и
  Арендатором.

  Апартамент – Помещение, передаваемое во временное владение и пользование Арендатору для проживания
  Арендатора и Жильцов в соответствии с Договором аренды.

  Арендатор – лицо заключившее договор аренды с Арендодателем. Арендодатель – ООО «Апартсервис», заключающее с
  Арендатором договор аренды/ договор субаренды, называющийся анкетой-договором.

  Гость – любой гость арендатор и/или Жильца, включая личных гостей, обслуживающий персонал, врачей, курьеров
  и иных временных посетителей.

  Жилец – Арендатор и/или иное физическое лицо, проживающее в Апартаменте на основании договора Аренды.

  Комплекс – Здание, а также места общего пользования, в том числе придомовая территория, в котором
  располагается Апартамент.

  Места общего пользования – все помещения, в том числе придомовая территория, Комплекса, предназначенные для
  общего пользования в свободном и равноправном доступе всех посетителей и жильцов Комплекса.

  Пользователи – лица, которые в соответствии с Договором аренды имеют право пользования Апартаментом.

  Правила – настоящие правила проживания и пользования.

  Прейскурант дополнительных услуг (прейскурант) – документ, принимаемый Арендодателем в одностороннем
  порядке, который содержит перечень услуг, оказываемых Арендодателем лично или с привлечением третьих лиц за
  отдельную плату Арендатору.

  Принадлежности – бытовая техника, предметы мебели и интерьера, находящиеся в Апартаменте.

  Сервисная компания – компания, осуществляющая управление и техническую эксплуатацию Комплекса.

  <strong><span id="2">2. Бронирование и отмена бронирования</span></strong>

  1.1. Бронирование услуг производится доступным пользователю способом. Обязательная информация для
  бронирования: - ФИО, контактный телефон, электронная почта - Дату заезда/выезда; - Количество прибывающих
  гостей; - Удобный способ оплаты услуг; -

  1.2. Гарантированное бронирование - производится путем подтвержденной оплаты любым удобным гостю способом на
  счет агентства в размере тарифа стоимости 1 суток пребывания, подтверждается гарантией агентства обеспечить
  гостю забронированную квартиру до наступления расчетного времени в следующий после прибытия день 11:00.

  1.3. Возврат оплаты за гарантированное бронирование производится полностью в случае, если отмена прошла не
  позднее 24 часов до расчетного дня заезда.
  1.4. Возврат оплаты за гарантированное бронирование не производится в случае, если отмена прошла позднее 24
  часов до расчетного дня заезда.
  1.5. Отмена гарантированного бронирования производится через официальный сайт агентства www.aeroapart.ru
  1.6. Способы оплаты гарантированного бронирования услуг - Картой VISA, MasterCard через модуль размещенный
  на сайте агентства www.aeroapart.ru

  <strong><span id="3">3. Депозит</span></strong>

  3.1. При заключении договора Арендатор вносит Депозит в размере и на условиях указанный в анкете-договоре.

  3.2. Депозит оплачивается банковской картой при подписании Анкеты-договора.

  3.3. На период действия анкеты-договора Депозит находится в распоряжении Арендодателя с согласия Арендатора
  и на сумму депозита не подлежат начислению какие-либо проценты, в том числе установленные законом.

  3.4. Арендодатель вправе удерживать денежные средства Депозита частично или полностью при нарушении
  Арендатором настоящий Правил и условий Анкеты-договора, в том числе зачитывать денежные средства Депозита в
  счет задолженности Арендатора перед Арендодателем без согласия на это Арендатора.

  3.5. Депозит возвращается Арендатору при подписании акта приема-передачи в день окончания действия Анкеты
  договора.

  <strong><span id="4">4. Тарифы и прейскурант дополнительных услуг</span></strong>

  4.1. Действующие Тарифы на Апартамент размещены на сайте Арендодателя www.aeroapart.ru.

  4.2. Прейскурант дополнительных услуг размещен на сайте Арендодателя www.aeroapart.ru.

  <strong><span id="5">5. Осмотр Апартамента и получение ключей Арендатором</span></strong>

  5.1. Осмотр Апартамента и передаваемых Арендатору Принадлежностей производится в соответствии с положениями
  Договора аренды и настоящих Правил.

  5.2. Осмотр Апартамента при заселении производится в предварительно согласованное сторонами время после
  заключения Договора аренды и исполнения Арендатором обязательств по внесению обязательных платежей в
  соответствии с условиями Договора аренды.

  5.3. При осмотре обязательно личное присутствие Арендатора или представителя Арендодателя.

  5.4. Осмотр Апартамента сопровождается составлением и подписанием акта приема-передачи Апартамента и
  Принадлежностей, а при отсутствии подписанного сторонами акта приема-передачи силу акта имеет Анкетадоговор.

  5.5. Каждый Апартамент имеет следующие принадлежности: см.Комплектация
  5.6. В день регистрации заезда Арендатору выдается ключ (без права передоверия третьим лицам) от запорного
  устройства и электронная карта доступа, не более двух экземпляров на Апартамент. Вид запорного устройства по
  соглашению Сторон значения не имеет, в том числе ключ и запорное устройство могут быть электронными

  5.7. Арендатор не имеет права самостоятельно заменять запорные механизмы Апартамента.

  5.8. Арендатор обязан заявить о дефектах Апартамента и Принадлежностей в случае наличия таковых до
  подписания акта приема-передачи Помещения и заселения. Указанные дефекты отражаются в акте-приема передачи и
  устраняются в соответствии с условиями Договора аренды.

  5.9. В случае отсутствия замечаний к состоянию Помещения Арендатор перед заселением обязан подписать Акт
  приема-передачи Апартамента с перечнем передаваемых Принадлежностей, в том числе ключей и электронных карт
  доступа.

  5.10. Заключение Договора аренды и передача Апратмента и Принадлежностей Арендатору возможны только после
  предоставления Арендатором необходимых документов, требуемых по Договору аренды, и исполнения Арендатором
  всех обязанностей по внесению обязательных платежей в соответствии с условиями Договора аренды.

  <strong><span id="6">6. Порядок заселения/выселения</span></strong>

  6.1 Заселение в Апартамент производится Арендатором самостоятельно на основании онлайн регистрации в
  предварительно согласованное сторонами время, в период с 14:00 до 00:00, после подписания сторонами Договора
  аренды, внесения Арендатором обязательных платежей, предусмотренных Договором аренды, и подписания Акта
  приема-передачи Апартамента и перечня передаваемых Принадлежностей. По соглашению сторон возможно заселение
  ранее указанного периода, с увеличением стоимости согласно действующих тарифов. Порядок выселения Выселение
  из Апартамента производится Арендатором самостоятельно, не позднее 11 часов 00 минут даты окончания срока
  аренды в соответствии с Договором аренды. По соглашению сторон возможно выселение позднее указанного
  времени, при условии увеличения стоимости согласно действующий тарифов. На момент выселения Арендатор обязан
  передать освобождаемый Апартамент вместе со всеми Принадлежностями Арендодателю в надлежащем состоянии и
  комплектации, без дефектов и повреждений, а именно в том же состоянии, в котором они были получены
  Арендатором на Дату передачи по акту приема Предмета аренды. На момент подписания Акта возврата Предмета
  аренды все задолженности Арендатора по оплате, а также иные финансовые обязательства, возникшие у Арендатора
  в связи с арендой, должны быть надлежащим образом исполнены. Все личное имущество Арендатора и Пользователей
  Апратамента, а также мусор и прочее, должны быть удалены из Апартамента и Мест общего пользования. Арендатор
  обязуется вернуть все полученные ключи – как электронные, так и механические – от Апартамента, входа в
  подъезд, а также прочие ключи в дату окончания Договора аренды.

  <strong><span id="7">7. Пользование Апартаментом</span></strong>

  7.1. Апартамент используются только для временного проживания в соответствии с Разрешенным использованием.

  7.2. Пользование Апартаментом осуществляется с учетом соблюдения прав и законных интересов проживающих
  поблизости третьих лиц, гостей и жильцов, требований пожарной безопасности, санитарно-гигиенических,
  экологических и иных требований законодательства и Договора аренды.

  7.3. Апартамент может быть использован для осуществления Арендатором надомной работы, не связанной с
  производством, проведением шумных работ, приемом большого количества посетителей и при условии, что такая
  работа не причиняет дискомфорта, не затрагивает интересы других Пользователей, третьих лиц и не нарушает
  действующее законодательство, Договор аренды и настоящие Правила.

  7.4. Арендатору и Жильцам запрещается использовать Апартамент для ведения коммерческой деятельности,
  проживания лиц, не указанных в Договоре аренды, использовать Апартамент любым иным образом, кроме
  предусмотренного Договором аренды.

  7.5. Использование Апартамента в нарушение разрешенного Договором аренды использования и установленного
  законом и настоящими Правилами порядка влечет применение Арендодателем в отношении Арендатора санкций.

  7.6. Содержание домашних животных в Апартаменте допускается по специальному тарифу.

  7.7. Арендатор и Жильцы обязаны содержать Апартамент в чистоте и порядке, обеспечивать сохранность
  Апартамента и Принадлежностей, отделки, мебели, техники, элементов дизайна, предметов интерьера и иного
  имущества Арендодателя, обеспечивать работоспособность бытовой техники и мебели в процессе проживания,
  бережно относиться к занимаемому Апартаменту, санитарно-техническому и иному оборудованию, Местам общего
  пользования, соблюдать настоящие Правила, правила пожарной безопасности, правила пользования
  санитарно-техническим оборудованием и электробытовыми приборам.

  7.8. Арендатор и иные Пользователи обязаны использовать сантехническое оборудование только по его прямому
  назначению, не спускать в сливные отверстия (раковины, унитазы, стиральные машины и т.д.) для сбрасывания в
  канализацию предметы, способствующие нарушению работы системы канализации и образованию засоров (памперсы,
  одноразовые пеленки и пр.), ядовитые и вредные химические вещества, а также различные твердые предметы, изза
  сброса которых может произойти нарушение работы системы канализации или превышение допустимых норм
  концентрации вредных веществ в канализационных стоках.

  7.9. Любая порча сантехнического оборудования, связанная с ненадлежащим использованием, приведшим к выходу
  из строя или засору, оплачивается Арендатором.

  7.10. Пользователь обязан содержать лоджии/балконы, если таковые имеются в Апартаменте в чистоте.
  Запрещается загромождать лоджии вещами, оборудованием и т.п. Запрещается устанавливать любое оборудование на
  лоджии/балконы.

  7.11. Для проветривания Апартамента разрешается открывание окон только в режиме вертикального проветривания,
  при котором основное давление створки окна оказывается на основание оконного блока, а не на боковые петли.
  Проветривание апартамента при ином способе открывания окон запрещается.

  7.12. Запрещается использовать тонированные покрытия, пленки и защитные экраны, а также размещать любого
  рода информацию (плакаты, щиты, транспаранты, объявления и т.п.) на окнах, остекленных поверхностях и
  витражах Помещения и Мест общего пользования.

  7.13. Запрещается проносить, хранить или использовать на территории Апартамента и Мест общего пользования
  любые вещи (в том числе вещества), которые являются или могут оказаться опасными для здоровья, причинить
  вред Помещениям и Местам общего пользования, Принадлежностям или находящимся в них людям, в том числе
  легковоспламеняющиеся, горючие, радиоактивные, взрывоопасные, ядовитые, загрязняющие или отравляющие
  вещества, любое оружие и боеприпасы.

  7.14. Запрещается использовать в своих целях и интересах инвентарь и оборудование, принадлежащие
  Арендодателю, Сервисной компании, иным организациям, обслуживающим Комплекс и Объект.

  7.15. Запрещаются установка внешнего оборудования, антенн, в том числе спутниковых, дополнительных блоков
  систем кондиционирования, а также любое иное видоизменение фасадов здания и видоизменение Мест общего
  пользования.

  7.16. Любое переустройство и перепланировка Апартамента, изменение функционального назначения, изменение
  конструктивных параметров Апартамента и/или архитектурно-планировочных решений, а также изменение
  направления открывания двери, замена запорного механизма категорически запрещены.

  7.17. Пользователям запрещается вмешиваться в работу индивидуальных приборов учета (счетчиков) и
  автоматизированных систем контроля учета потребления коммунальных ресурсов.

  7.18. Запрещается производство любых работ: – с применением оборудования и инструментов, вызывающих
  превышение нормативно допустимого уровня шума и вибраций; - сопряженных с шумом ранее 09:00, в период с
  13:00 до 15:00 (перерыв в целях обеспечения дневного сна детей), после 19:00 в будние дни; – в выходные дни,
  а также нерабочие общегосударственные праздничные дни.

  7.19. Не допускается выполнение работ или совершение других действий, приводящих к порче Апартамента и/или
  Мест общего пользования.

  7.20. Уровень шума не должен превышать установленных нормативами величин. Пользование музыкальными
  инструментами, телевизорами, радиоприемниками, магнитофонами и другими громкоговорящими устройствами
  допускается лишь при условии уменьшения громкости до степени, не нарушающей покоя других Пользователей и/или
  третьих лиц.

  7.21. В будние, выходные и нерабочие общегосударственные праздничные дни с 23.00 до 9.00 часов и с 13.00 до
  15.00 должна соблюдаться тишина.

  7.22. Запрещается самостоятельный ремонт Принадлежностей (бытовой техники, оборудования и мебели),
  являющихся собственностью Арендодателя.

  7.23. Запрещается производить демонтаж установленного оборудования и встроенной мебели. Расходы по
  устранению ущерба, нанесенного Апартаменту, Зданию по вине Арендатора, Пользователей или гостей/посетителей
  Арендатора в результате ненадлежащего использования оборудования или мебели, возмещаются самим Арендатором.

  7.24. Пользователи обязаны допускать в занимаемый Апартамент представителей Сервисной компании и
  Арендодателя в соответствии с Договором аренды и настоящими Правилами для осмотра технического и санитарного
  состояния помещения, санитарно-технического и иного инженерного оборудования, находящегося в нем, проверки
  показаний приборов учета, а также для выполнения необходимых аварийных или ремонтно-профилактических работ в
  отношении общедомовых инженерных систем и коммуникаций и оказания Пользователю дополнительных услуг.
  Сервисная компания вправе требовать допуск в заранее согласованное время, но не чаще 1 (одного) раза в 1
  (один) месяц, в занимаемое Арендатором Помещение представителей Сервисной компании (в том числе работников
  аварийных служб) для осмотра технического и санитарного состояния оборудования, для выполнения необходимых
  ремонтных работ и проверки устранения недостатков – по мере необходимости, а для ликвидации аварий – в любое
  время.

  7.25. Сервисная компания вправе требовать от Арендатора полного возмещения убытков, возникших по его вине, в
  случае отказа в допуске в занимаемый им Апартамент представителей Сервисной компании или Арендодателя, в том
  числе в случае невозможности исполнения своих обязательств по выполнению Заявок Арендатора и оказания ему
  дополнительных услуг.

  7.26. В экстренных случаях, когда существует угроза причинения повреждений Апартаменту и Принадлежностям (в
  том числе в случае пожара, затопления, разрушения Апартамента), а также жизни и здоровью людей, Арендодатель
  вправе посещать Помещение в любое время без согласования с Арендатором и Жильцами, для этих целей
  Арендодатель вправе хранить у себя комплект ключей от Апартамента.

  7.27. Каждый 5-ый день действия Анкеты-договора силами Арендодателя в период с 10:00 до 20:00 производится
  уборка Апартамента. Арендатор обязан обеспечить возможность проведения уборки. О времени уборки Арендатора
  уведомляет Администратор в день уборки.

  <strong><span id="8">8. Пользование Местами общего пользования</span></strong>

  8.1. Места общего пользования, а также иные объекты общего имущества в Комплексе и Объекте используются
  Пользователями только в тех целях, для которых они предназначены.

  8.2. Арендатор обязан бережно относиться к Местам общего пользования и общему имуществу Комплекса (объектам
  благоустройства и зеленым насаждениям, детским площадкам), не допускать их порчи и загрязнения,
  предпринимать все возможные меры к их сохранности.

  8.3. В Местах общего пользования не разрешается вести какую-либо производственную, коммерческую,
  предпринимательскую или рекламную деятельность, не связанную с проживанием в Комплексе, а также вести
  политическую или религиозную агитацию и пропаганду. В частности, запрещается оставлять какие-либо
  информационные сообщения на стенах, дверях, потолках, в кабинах лифтов и на любых иных поверхностях Мест
  общего пользования, в том числе на входной двери в Помещение.

  8.4. Запрещено хранение или оставление каких-либо предметов в Местах общего пользования, в том числе в
  этажных и лифтовых холлах (например, велосипедов, самокатов, детских колясок, обуви, мебели, пустых коробок,
  мусорных пакетов и пр.), за исключением мест, отведенных под соответствующие цели.

  8.5. Весь бытовой мусор и пищевые отходы должны утилизироваться в специально отведенных для этого местах и в
  специально отведенной упаковке (мусорный мешок).

  8.6. Весь крупногабаритный мусор и отходы должны утилизироваться арендатором самостоятельно в специально
  отведенные контейнеры для крупногабаритного мусора и твердых бытовых отходов, установленные на улице на
  оборудованных площадках.

  8.7. Разведение цветов и других комнатных декоративных растений возможно только внутри арендуемого
  Помещения. Содержание домашних животных в Местах общего пользования не допускается.

  8.8. Пользователям запрещается: - использовать места общего пользования и технические помещения Комплекса
  для организации производственных участков, мастерских, а также хранения продукции, оборудования, мебели и
  других предметов личного пользования; - производить действия, в результате которых ухудшаются условия
  безопасной эвакуации людей; - загромождать лестничные клетки мебелью, вещами, горючими материалами,
  оборудованием и другими предметами; - устанавливать телевизионные и иные антенны и устройства, кондиционеры,
  электрические и телефонные провода на стенах или крыше дома, в других местах, относящихся к Местам общего
  пользования Комплекса; - курить, в том числе кальян, а также использовать зажжённые свечи, на лестничных
  площадках, в коридорах, лифтовых холлах и лифтах, на детских и спортивных площадках; - находиться в
  состоянии алкогольного или наркотического опьянения на территории Мест общего пользования, Прилегающей
  территории; - вести агитационную деятельность, проводить несанкционированные мероприятия, гуляния, концерты
  на территории Комплекса и прилегающей территории; - вывешивать объявления на лестничных клетках, лифтах,
  дверях, стенах; - выходить на крышу, технический этаж и в иные технические и служебные помещения; -
  парковать транспортные средства на газонах, детских площадках, пешеходных дорожках, в местах расположения
  противопожарных проездов, в зоне контейнерных площадок и иных местах, не предназначенных для этой цели; -
  мыть транспортные средства и ремонтировать их на придомовой территории; - осуществлять стоянку и хранение
  ветхих и сломанных транспортных средств, а также стоянку грузовых и специальных автомобилей
  грузоподъемностью свыше 1,5 тонн и автобусов вместимостью свыше 10 человек на придомовой территории; -
  производить самовольную вырубку кустов и деревьев, срезку цветов и любые действия, вызывающие нарушение
  травяного покрова газонов; самовольную установку ограждений, гаражей и любых построек.

  8.9. В случае нанесения ущерба Местам общего пользования и общему имуществу Комплекса транспортным средством
  владелец транспортного средства обязан компенсировать за свой счет ремонт поврежденных объектов Мест общего
  пользования и общей собственности.

  8.10. Пользователи обязаны соблюдать чистоту и порядок в подъездах, на лестничных клетках, придомовой
  территории и в других Местах общего пользования.

  8.11. При получении информации от Сервисной компании о проведении работ по очистке и уборке территории
  Пользователь, имеющий транспортное средство, обязан обеспечить его перемещение для беспрепятственного и
  безопасного проведения этих работ. О дате проведения работ Сервисная компания информирует Арендаторов
  заблаговременно.

  <strong><span id="9">9. Запрет курения</span></strong>

  Принимая в особое внимание оснащение Комплекса и Апартамента системой пожарной сигнализации и
  автоматического водяного пожаротушения, а также в соответствии с действующим законодательством Российской
  Федерации (Федеральный закон от 23.02.2013 № 15-ФЗ «Об охране здоровья граждан от воздействия окружающего
  табачного дыма и последствий потребления табака») курение, в том числе курение кальяна и использование
  зажженных свечей, на территории Комплекса, в Апартаменте, на лоджиях и в Местах общего пользования строго
  запрещено. Несоблюдение запрета курения является грубым нарушением условий Договора аренды и настоящих
  Правил. При обнаружении случаев курения Арендодатель или Сервисная компания вправе применить в отношении
  Арендатора и Пользователей соответствующие санкции, предусмотренные Договором аренды.

  <strong><span id="10">10. Соблюдение правил пожарной безопасности</span></strong>

  10.1. Арендатор является ответственным лицом за соблюдение правил пожарной безопасности, определенных
  законодательством Российской Федерации, в арендуемом Помещении. В случае, если при осмотре Помещения
  (Арендодателем, Сервисной компанией) будут выявлены нарушения, Арендатор обязан принять меры к их
  устранению. Арендатор, Жильцы и Пользователи обязаны соблюдать требования пожарной безопасности на всей
  территории Комплекса, в Местах общего пользования и в Апартаменте, соблюдать меры предосторожности при
  пользовании электроприборами, предметами бытовой химии, не производить работ с легковоспламеняющимися и
  горючими жидкостями и другими опасными в пожарном отношении веществами, материалами и оборудованием.

  10.2. В рамках соблюдения правил пожарной безопасности: - Арендатор несет ответственность за ложные
  срабатывания систем пожарной безопасности, произошедшие по вине Арендатора, Жильцов и Пользователей,
  проживающих или находящихся в Помещении, а также за все последствия, наступившие в результате такого ложного
  срабатывания; - Арендодатель оставляет за собой право изменять и дополнять системы безопасности по мере
  необходимости; - Арендатору и Жильцам в случае сигнала тревоги при чрезвычайной ситуации любого характера
  надлежит эвакуироваться с этажа, на котором расположены Помещения, или при необходимости покинуть Объект, а
  при срабатывании автоматической системы речевого оповещения и управления эвакуацией действовать согласно
  транслируемым системой инструкциям; - Арендатор и Жильцы обязаны должным образом принимать участие во всех
  мероприятиях по отработке действий в чрезвычайных ситуациях, организуемых Сервисной компанией; - Арендатор и
  Жильцы обязаны предоставлять уполномоченным представителям Сервисной компании доступ в Помещение с целью
  проведения регулярных проверок или ремонта любых систем в соответствии с Договором аренды и настоящими
  Правилами; - Арендатор и Жильцы обязаны следить за целостностью и исправностью противопожарного оборудования
  и устройств пожарной сигнализации (ручных пожарных извещателей, датчиков дыма и т.д.) и устройств системы
  автоматического водяного пожаротушения в Помещениях и в случае обнаружения неисправности или нарушения
  целостности противопожарного оборудования незамедлительно сообщить об этом сотруднику Арендодателя или
  Сервисной компании.

  10.3. В Помещениях категорически запрещается: - курить, разводить огонь, в том числе использовать
  пиротехнические средства (свечи, хлопушки, фейерверки); - хранить легковоспламеняющиеся и горючие жидкости,
  взрывчатые вещества, баллоны с газами и другие взрывоопасные вещества и материалы; - проводить уборку
  Помещения с применением бензина, керосина и других легковоспламеняющихся и горючих жидкостей; - оставлять
  без присмотра включенные в сеть электроприборы, телевизоры, радиоприемники, компьютеры, принтеры и пр.; -
  самостоятельно прокладывать транзитные кабельные линии и электропроводку в Помещениях и Местах общего
  пользования; - использовать имеющиеся средства пожаротушения не по прямому назначению; - открывать
  электрощиты и производить любые работы без согласования работ с Сервисной компанией; - пользоваться
  неисправными светильниками, электровыключателями, электророзетками и штепсельными вилками с признаками
  нарушения целостности корпуса, его частей либо отсутствующими комплектующими, обгоревшими и закопченными
  контактами, с ненадежно закрепленными искрящими и нагревающимися в месте контакта проводами; - применять на
  территории Комплекса, Объекта и в непосредственной близости от них свечи и хлопушки, зажигать фейерверки и
  устраивать другие световые эффекты, которые могут привести к пожару; - производить любое несанкционированное
  вмешательство в работу автоматических средств противопожарной защиты, систем оповещения о пожаре,
  автоматических устройств обнаружения пожара (автоматическую пожарную сигнализацию), установок
  автоматического водяного пожаротушения; - бросать горючие или опасные материалы в мусоропроводы; -
  загромождать мебелью, материалами и оборудованием пути эвакуации (коридоры, лестничные клетки, вестибюли,
  тамбуры эвакуационных выходов из зданий, лифтовые холлы), доступ к первичным средствам пожаротушения,
  электрораспределительным щитам и отключающим устройствам, не затруднять доступ к огнетушителям или пожарным
  кранам/шлангам; - использовать электроприборы, потребляемая мощность которых превышает допустимую
  потребляемую мощность электросети (электрочайники, электрообогреватели и иное мощностью более 2 кВт), а
  также включать в электросеть одновременно несколько электроприборов, суммарная потребляемая мощность которых
  превышает допустимую; - закрывать вытяжные каналы, отверстия и решетки; - пользоваться нагревательными
  электроприборами для отопления помещений, приготовления и разогрева пищи вне специально отведенных мест и
  зон; - устанавливать и изменять в Помещении любые охранные системы безопасности; - подавать ложные сигналы о
  возгорании.

  <strong><span id="11">11. Права, обязанности и ответственность Арендодателя</span></strong>

  11.1. Арендодатель имеет право в одностороннем порядке менять Настоящие Правила. Об изменениях Правил
  Арендодатель уведомляет Арендаторов путем размещения Правил и на сайте Арендодателя www.aeroapart.ru.

  11.2. Арендодатель обязуется обеспечить сохранность персональных данных Арендатора и Жильцов.

  11.3. Арендодатель несет ответственность за состояние имущества Пользователей только в случаях намеренной
  порчи или грубой неосторожности со своей стороны.

  11.4. Арендодатель не несет ответственности за сохранность личного имущества Пользователей, находящегося в
  Апартаменте, а также оставленного без присмотра в Местах общего пользования или придомовой территории.

  <strong><span id="12">12. Права, обязанности и ответственность Арендатора</span></strong>

  12.1. Права и обязанности Арендатора установлены Договором и настоящими Правилами. Вместе с тем Арендатор
  обязуется: - уведомлять Арендодателя об изменениях в списке Жильцов; - заблаговременно уведомлять
  Арендодателя о своем длительном отсутствии более двух суток; - не производить изменений в комплектации
  занимаемого апартамента без предварительного уведомления Арендодателя; - неукоснительно соблюдать настоящие
  Правила; - сообщать Арендодателю обо всех фактах нарушения и несоблюдения Правил другими Пользователями.

  12.2. За утерю/утрату любого ключа Арендатор обязан уплатить штраф 1000 руб.;

  12.3. За любое нарушение пунктов 8,9,10 настоящий правил Арендатор обязан уплатить штраф в размере 5000 руб.

  12.4. Уплата штрафа не освобождает Арендатора от обязанности возместить реальный ущерб.

  <strong><span id="13">13. Ответственность за несоблюдение Правил</span></strong>

  13.1. При несоблюдении настоящих Правил Арендатор и Жильцы несут ответственность в соответствии с
  действующим законодательством Российской Федерации, Договором аренды. Документом, подтверждающим факт
  нарушения Правил, является Акт о нарушении, составленный уполномоченным представителем Арендодателя и/или
  Сервисной компании в присутствии представителей Арендатора и не менее двух граждан указанием даты и времени
  совершения нарушения, а также подтверждением нарушения подписями свидетелей или при необходимости ссылкой на
  данные видеонаблюдения или фотографии.

  13.2. На основании Акта о нарушении Арендатор может быть привлечен к ответственности за несоблюдение Правил
  и ненадлежащее исполнение условий Договора аренды, в том числе влекущей за собой право Арендодателя на
  досрочное прекращение Договора аренды.

  13.3. Арендодатель имеет право на досрочное прекращение Договора аренды и взыскание с Арендатора прямого
  ущерба и понесенных убытков, в том числе в случаях: - нарушения Арендатором Запрета на Курение в Помещениях,
  на лоджиях, на лестничных клетках и в Местах общего пользования; - нарушения Правил пожарной безопасности; -
  выполнения самовольного ремонта и перепланировки Помещений.

  <strong><span id="14">14. Досрочное выселение</span></strong>

  Арендодатель вправе приостановить возможность доступа Арендатора и Жильцов в Апартамент, досрочно прекратить
  Договор аренды и организовать выселение Арендатора и Жильцов в случае нарушения Арендатором или Жильцами
  настоящих Правил, Договора Аренды, в частности: - в случае нарушения обязательств по внесению Арендной платы
  в соответствии с Договором о присоединении к сервису; - в случае нарушения обязательств по оплате в
  соответствии с Договором Договором о присоединении к сервису; - в случае нарушения Арендатором или Жильцами
  запрета на курение в соответствии с настоящими Правилами; - в случае нарушения Арендатором или Жильцами
  Правил пожарной безопасности, установленных настоящими Правилами; - в случае нарушения Арендатором или
  Жильцами запрета на парковку на территории Комплекса, если таковые имеются; - в случае нарушения Арендатором
  общественного порядка; - в случае несоблюдения Арендатором или Жильцами условий настоящих Правил о
  допустимом уровне шума в Апартаменте и Комплексе; - в случае нарушения Арендатором или Жильцами запрета на
  перепланировку или переоборудование Апаратамента, установленного настоящими Правилами и Договором о
  присоединении к сервису; - в случае разрушения или нанесения существенного ущерба Помещению или Общему
  имуществу по вине Арендатора или Жильцов.


  <strong><span id="pay">Об оплате</span></strong><br>
  <b>Политика конфеденциальности, мы предоставляем:</b>
  • неразглашение персональных данных и их безопасность;
  • использование сведений, предоставленных пользователем, исключительно с целью связи с ним, в том числе для
  направления электронных уведомлений.

  <b>Способы оплаты:</b>
  Банковской картой
  Для выбора оплаты товара с помощью банковской карты на соответствующей странице необходимо нажать кнопку
  «Оплата заказа банковской картой».
  Оплата происходит через ПАО СБЕРБАНК с использованием Банковских карт следующих платежных систем:
  • МИР (разместить логотип МИР)
  • VISA International (разместить логотип VISA International)
  • Mastercard Worldwide (разместить логотип Mastercard Worldwide)
  • JCB (разместить логотип JCB)
  Для оплаты (ввода реквизитов Вашей карты) Вы будете перенаправлены на платежный шлюз ПАО СБЕРБАНК.
  Соединение
  с платежным шлюзом и передача информации осуществляется в защищенном режиме с использованием протокола
  шифрования SSL. В случае если Ваш банк поддерживает технологию безопасного проведения интернет-платежей
  Verified
  By Visa, MasterCard SecureCode, MIR Accept, J-Secure для проведения платежа также может потребоваться ввод
  специального пароля.
  Настоящий сайт поддерживает 256-битное шифрование. Конфиденциальность сообщаемой персональной информации
  обеспечивается ПАО СБЕРБАНК. Введенная информация не будет предоставлена третьим лицам за исключением
  случаев,
  предусмотренных законодательством РФ. Проведение платежей по банковским картам осуществляется в строгом
  соответствии с требованиями платежных систем МИР, Visa Int., MasterCard Europe Sprl, JCB`
