export default {
	contract: 'Договор',
	dialog: {
		title: 'Договор о присоединении',
	},
	accepted: 'Договор принят, создан новый апартамент',
	declined: 'Договор отклонён и отправлен собственнику',
	filter: {
		all: 'Все',
		verification: 'Ожидает проверки',
		accepted: 'Принят',
		declined: 'Отклонён',
	},
	show_apartment: 'Показать апартамент',
}
