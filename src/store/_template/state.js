export default function () {
	return {
		editDialog: false,
		formLoading: false,

		itemLoading: false,
		item: {},

		items: [],
		loading: false,

		error: null,
		errors: {},
	}
}
