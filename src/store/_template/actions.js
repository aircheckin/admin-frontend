import * as crud from '@/utils/crud'
import { SET_ERRORS, SET_FORM_LOADING, SET_ITEM_LOADING, SET_LOADING } from './mutation_types'
import { url } from './index'
import axios from '@/plugins/axios'
import Vue from 'vue'
import i18n from '@/i18n'


export const getItem = ({ dispatch, commit }, id) =>
	crud.getItem({ dispatch, commit }, url, id)

export const getItems = ({ dispatch, commit }, query) =>
	crud.getItems({ dispatch, commit }, url, query)

export const create = ({ dispatch, commit, state }) =>
	crud.create({ dispatch, commit, state }, url)

export const update = ({ dispatch, commit, state }) =>
	crud.update({ dispatch, commit, state }, url)

export const setLoading = ({ commit }, value) => commit(SET_LOADING, value)

export const setItemLoading = ({ commit }, value) => commit(SET_ITEM_LOADING, value)

export const remove = ({ dispatch, commit }, item) =>
	crud.remove({ dispatch, commit }, url, item)

export const reset = ({ commit }) => crud.reset({ commit })

export const customMethod = ({ commit }, address) => {
	commit(SET_FORM_LOADING, true)

	return axios.get('path/to-action', { params: { address } })
		.then(({ data }) => {
			console.log(data)
			Vue.prototype.$snotify.success(i18n.tc('data.recieved'))
		})
		.catch(({ response }) => {
			const { data: { message } } = response

			commit(SET_ERRORS, { address: ' ' })
			Vue.prototype.$snotify.error(message)
		})
		.finally(() => {
			commit(SET_FORM_LOADING, false)
		})
}
