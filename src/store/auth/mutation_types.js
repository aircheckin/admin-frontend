export const SET_LOADING = 'SET_LOADING'
export const SET_TOKEN = 'SET_TOKEN'
export const SET_USER = 'SET_USER'
export const SET_LOGGED_IN = 'SET_LOGGED_IN'
export const SET_VERIFIED = 'SET_VERIFIED'
export const LOGOUT = 'LOGOUT'
