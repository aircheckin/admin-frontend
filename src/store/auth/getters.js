export { getField } from 'vuex-map-fields'

export const user = state => state.user

export const hasRole = state => roles => {
	if (state.loggedIn) {

		if (typeof roles === 'string') {
			roles = roles.split(',')
		}

		return roles.some(r => state.user.roles.indexOf(r) >= 0)
	}

	return false
}

export const hasPermission = state => permission => {
	return state.loggedIn && state.user.permissions.indexOf(permission) >= 0
}
