export default function () {
	return {
		token: localStorage.getItem('token') ?? '',
		user: {},
		loggedIn: false,
		verified: false,
		loading: false,
	}
}
