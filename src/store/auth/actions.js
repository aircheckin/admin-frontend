import { LOGOUT, SET_LOADING, SET_TOKEN, SET_USER } from './mutation_types'
import axios from '@/plugins/axios'
import router from '@/router'

export const setLoading = ({ commit }, value) => commit(SET_LOADING, value)

export const login = ({ commit }, params) => {
	commit(SET_LOADING, true)

	axios.post('create-token', params)
		.then(({ data: { token } }) => {
			commit(SET_TOKEN, token)
			router.push('/')
		})
		.finally(() => {
			commit(SET_LOADING, false)
		})
}

export const getInitData = ({ commit }) => {
	return new Promise((resolve, reject) => {
		axios.get('init-data')
			.then(({ data: { user, owners, booking_sources, payment_methods } }) => {
				commit(SET_USER, user)
				commit('owners/SET_ITEMS', owners, { root: true })
				commit('global/SET', {
					bookingSources: booking_sources,
					paymentMethods: payment_methods,
				}, { root: true })
				resolve(user)
			})
			.catch(() => {
				reject()
			})
	})
}

export const logout = ({ commit }) => {
	axios.post('user/logout').finally(() => {
		commit(LOGOUT)
		router.push('login')
	})
}
