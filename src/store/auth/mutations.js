import * as types from './mutation_types'
import defaultState from './state'
import { updateField } from 'vuex-map-fields'

export default {
	updateField,
	[types.SET_LOADING] (state, value) {
		state.loading = value
	},

	[types.SET_TOKEN] (state, token) {
		localStorage.setItem('token', token)
		state.token = token
	},

	[types.SET_USER] (state, user) {
		state.user = user

		if (user.email_verified_at) {
			state.verified = true
		}

		state.loggedIn = true
	},

	[types.SET_LOGGED_IN] (state, value) {
		state.loggedIn = value
	},
	[types.SET_VERIFIED] (state, value) {
		state.verified = value
	},

	[types.LOGOUT] (state) {
		localStorage.removeItem('token')
		Object.assign(state, defaultState())
	},
}
