export const SET_LOADING = `SET_LOADING`
export const SET_ITEM = `SET_ITEM`
export const UPDATE_ITEM = `UPDATE_ITEM`
export const SET_ITEMS = `SET_ITEMS`
export const SET_ERROR = `SET_ERROR`
export const SET_ERRORS = `SET_ERRORS`
export const RESET = `RESET`
