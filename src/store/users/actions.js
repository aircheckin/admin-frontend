import * as crud from '@/utils/crud'
import { SET_LOADING } from './mutation_types'
import { url } from './index'
import axios from '@/plugins/axios'
import Vue from 'vue'
import i18n from '@/i18n'

export const getItem = ({ dispatch, commit }, id) =>
	crud.getItem({ dispatch, commit }, url, id)

export const getItems = ({ dispatch, commit }, query) =>
	crud.getItems({ dispatch, commit }, url, query)

export const create = ({ dispatch, commit, state }) =>
	crud.create({ dispatch, commit, state }, url)

export const update = ({ dispatch, commit, state }) =>
	crud.update({ dispatch, commit, state }, url)

export const setLoading = ({ commit }, value) => commit(SET_LOADING, value)

export const remove = ({ dispatch, commit }, item) =>
	crud.remove({ dispatch, commit }, url, item)

export const reset = ({ commit }) => crud.reset({ commit })

export const invite = ({ dispatch }, userData) => {
	return axios.post('users/invite', userData)
		.then(() => {
			Vue.prototype.$snotify.success(i18n.tc('users.inviteSent'))
			dispatch('getItems')
		})
}
