import * as crud from '@/utils/crud'
import { SET_LOADING } from './mutation_types'
import { url } from './index'
import axios from '@/plugins/axios'

export const getItem = ({ dispatch, commit }, id) =>
	crud.getItem({ dispatch, commit }, url, id)

export const getItems = ({ dispatch, commit }, query) =>
	crud.getItems({ dispatch, commit }, url, query)

export const create = ({ dispatch, commit, state }) =>
	crud.create({ dispatch, commit, state }, url)

export const update = ({ dispatch, commit, state }) =>
	crud.update({ dispatch, commit, state }, url)

export const setLoading = ({ commit }, value) => commit(SET_LOADING, value)

export const remove = ({ dispatch, commit }, item) =>
	crud.remove({ dispatch, commit }, url, item)

export const reset = ({ commit }) => crud.reset({ commit })

export const getBookings = (_, { ownerId, query }) => {
	return axios.get('owners/' + ownerId + '/bookings', { params: query })
}

export const getReports = (_, ownerId) => {
	return axios.get('owners/' + ownerId + '/reports')
}

export const downloadContractReport = (_, reportId) => {
	return axios.get(`reports/contract/${reportId}/download`, {
		responseType: 'blob',
	}).then(({ data }) => {
		const url = window.URL.createObjectURL(new Blob([data]))
		const link = document.createElement('a')
		link.href = url
		link.setAttribute('download', 'report.pdf')
		document.body.appendChild(link)
		link.click()
	})
}

export const getNotifications = (_, ownerId) => {
	return axios.get('owners/' + ownerId + '/notifications')
}

export const createNotification = (_, { ownerId, message }) => {
	return axios.post('owners/' + ownerId + '/notifications', { message })
}

export const deleteNotification = (_, { ownerId, notificationId }) => {
	return axios.delete('owners/' + ownerId + '/notifications/' + notificationId)
}
