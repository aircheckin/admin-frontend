export default function () {
	return {
		loading: false,
		item: {
			isActive: true,
		},
		items: [],
		error: null,
		errors: {},
		bookings: {},
		reportsDialog: false,
		notificationsDialog: false,
	}
}
