import axios from '@/plugins/axios'

export const downloadTotalReport = (_, params) => {
	return axios.get(`reports/total/download`, {
		responseType: 'blob',
		params,
	}).then(({ data }) => {
		const url = window.URL.createObjectURL(new Blob([data]))
		const link = document.createElement('a')
		link.href = url
		link.setAttribute('download', 'report.pdf')
		document.body.appendChild(link)
		link.click()
	})
}
