import * as crud from '@/utils/crud'
import { SET_ITEM_LOADING, SET_LOADING } from './mutation_types'
import { url } from './index'
import axios from '@/plugins/axios'


export const getItem = ({ dispatch, commit }, id) =>
	crud.getItem({ dispatch, commit }, url, id)

export const getItems = ({ dispatch, commit }, query) =>
	crud.getItems({ dispatch, commit }, url, query)

export const create = ({ dispatch, commit, state }) =>
	crud.create({ dispatch, commit, state }, url)

export const update = ({ dispatch, commit, state }) =>
	crud.update({ dispatch, commit, state }, url)

export const setLoading = ({ commit }, value) => commit(SET_LOADING, value)

export const setItemLoading = ({ commit }, value) => commit(SET_ITEM_LOADING, value)

export const remove = ({ dispatch, commit }, item) =>
	crud.remove({ dispatch, commit }, url, item)

export const reset = ({ commit }) => crud.reset({ commit })

export const getRenters = (_, query = {}) => {
	return axios.get(url, { params: query })
}
