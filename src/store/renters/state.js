export default function () {
	return {
		editDialog: false,
		formLoading: false,

		itemLoading: false,
		item: {
			last_name: null,
			first_name: null,
			middle_name: null,
			phone: null,
			email: null,
			birth_date: null,
			citizenship: null,
			passport_number: null,
			passport_serial: null,
			passport_type: null,
		},

		items: [],
		loading: false,

		error: null,
		errors: {},
	}
}
