export { getField } from 'vuex-map-fields'
export const item = state => state.item
export const items = state => state.items
export const loading = state => state.loading
export const error = state => state.error
export const errors = state => state.errors
