export const SET_ITEM_LOADING = `SET_ITEM_LOADING`
export const SET_ITEM = `SET_ITEM`
export const SET_ITEM_VALUES = `SET_ITEM_VALUES`
export const UPDATE_ITEM = `UPDATE_ITEM`
export const SET_LOADING = `SET_LOADING`
export const SET_ITEMS = `SET_ITEMS`
export const SET_ERROR = `SET_ERROR`
export const SET_ERRORS = `SET_ERRORS`
export const SET_FORM_LOADING = `SET_FORM_LOADING`

export const RESET = `RESET`
