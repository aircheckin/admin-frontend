import * as crud from '@/utils/crud'
import { SET_FORM_LOADING, SET_ITEM_LOADING, SET_ITEM_VALUES, SET_LOADING } from './mutation_types'

import { url } from './index'
import axios from '@/plugins/axios'
import Vue from 'vue'
import i18n from '@/i18n'


export const getItem = ({ dispatch, commit }, id) =>
	crud.getItem({ dispatch, commit }, url, id)

export const getItems = ({ dispatch, commit }, query) =>
	crud.getItems({ dispatch, commit }, url, query)

export const create = ({ dispatch, commit, state }) =>
	crud.create({ dispatch, commit, state }, url)

export const update = ({ dispatch, commit, state }) =>
	crud.update({ dispatch, commit, state }, url)

export const setLoading = ({ commit }, value) => commit(SET_LOADING, value)

export const setItemLoading = ({ commit }, value) => commit(SET_ITEM_LOADING, value)

export const remove = ({ dispatch, commit }, item) =>
	crud.remove({ dispatch, commit }, url, item)

export const reset = ({ commit }) => crud.reset({ commit })

export const accept = ({ commit }, item) => {
	commit(SET_FORM_LOADING, true)

	return axios.post('owners/contracts/' + item.id + '/accept', { sign_date: item.sign_date })
		.then(({ data: { data } }) => {
			commit(SET_ITEM_VALUES, data)
			Vue.prototype.$snotify.success(i18n.tc('owners_contracts.accepted'))
		})
		.finally(() => {
			commit(SET_FORM_LOADING, false)
		})
}

export const decline = ({ commit }, item) => {
	commit(SET_FORM_LOADING, true)

	return axios.post('owners/contracts/' + item.id + '/decline')
		.then(({ data: { data } }) => {
			commit(SET_ITEM_VALUES, data)
			Vue.prototype.$snotify.success(i18n.tc('owners_contracts.declined'))
		})
		.finally(() => {
			commit(SET_FORM_LOADING, false)
		})
}
