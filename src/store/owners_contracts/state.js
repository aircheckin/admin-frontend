export default function () {
	return {
		editDialog: false,
		formLoading: false,

		itemLoading: false,
		item: {
			images: {},
		},

		status: {
			created: 1,
			verification: 2,
			declined: 3,
			accepted: 4,
		},

		items: [],
		loading: false,

		error: null,
		errors: {},
	}
}
