import * as crud from '@/utils/crud'
import { url } from './index'
import axios from '@/plugins/axios'
import Vue from 'vue'
import i18n from '@/i18n'


export const getItem = ({ dispatch, commit }, id) =>
	crud.getItem({ dispatch, commit }, url, id)

export const getItems = ({ dispatch, commit }, query) =>
	crud.getItems({ dispatch, commit }, url, query)

export const create = ({ dispatch, commit, state }) =>
	crud.create({ dispatch, commit, state }, url)

export const update = ({ dispatch, commit, state }) =>
	crud.update({ dispatch, commit, state }, url)

export const setLoading = ({ commit }, value) => commit('SET_LOADING', value)

export const setItemLoading = ({ commit }, value) => commit('SET_ITEM_LOADING', value)

export const remove = ({ dispatch, commit }, item) =>
	crud.remove({ dispatch, commit }, url, item)

export const reset = ({ commit }) => crud.reset({ commit })

export const setCancelled = ({ commit }, item) => {
	return axios.post(`${url}/${item.id}/cancel`).then(({ data: { data } }) => {
		commit('SET_ITEM_VALUES', data)
	})
}

export const setPaid = ({ commit }, item) => {
	return axios.post(`${url}/${item.id}/paid`).then(({ data: { data } }) => {
		commit('SET_ITEM_VALUES', data)
	}).then(() => Vue.prototype.$snotify.success(i18n.tc('bookings.status_changed')))
}

export const complete = ({ commit }, { item, hold_return_amount }) => {
	return axios.post(`${url}/${item.id}/complete`, { hold_return_amount }).then(({ data: { data } }) => {
		commit('SET_ITEM_VALUES', data)
		Vue.prototype.$snotify.success(i18n.tc('bookings.completed'))
	}).catch(e => {
		if (e.isProcessable) {
			const message = e.response.data.message ?? e.message
			Vue.prototype.$snotify.error(message)
			commit(`SET_ERROR`, message)
		}

		throw e
	})
}

export const downloadOrder = (_, item) => {
	return axios.get(`${url}/${item.id}/order/download`, {
		responseType: 'blob',
	}).then(({ data }) => {
		const url = window.URL.createObjectURL(new Blob([data]))
		const link = document.createElement('a')
		link.href = url
		link.setAttribute('download', 'booking_order.pdf') //or any other extension
		document.body.appendChild(link)
		link.click()
	})
}

