export default function () {
	return {
		editDialog: false,
		viewDialog: false,
		formLoading: false,

		itemLoading: false,
		item: {
			renter: {},
			phone: null,
			source_id: 1,
			apartment_id: null,
			date_from: null,
			date_to: null,
			day_price: null,
			period_price: null,
			deposit: null,
			cleaning_price: 800,
			cleanings_count: 1,
			payment_method_id: 1,
			service_agent_percent: 13.2,
			description: null,
		},

		items: [],
		loading: false,

		error: null,
		errors: {
			renter: {
				phone: null,
			},
			apartment_id: null,

		},
	}
}
