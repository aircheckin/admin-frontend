import * as actions from './actions'
import * as getters from './getters'
import mutations from './mutations'
import state from './state'

export const namespace = 'bookings'
export const url = 'bookings'

export default {
	namespaced: true,
	state,
	actions,
	getters,
	mutations,
}
