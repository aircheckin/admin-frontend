import axios from '@/plugins/axios'
import router from '@/router'

export default {
	namespaced: true,

	state: {
		processing: false,
		agreementModalShown: false,
		errors: {},
	},

	mutations: {
		agreementModalClose (state) {
			state.agreementModalShown = false
		},
		/*agreementModalShow (state) {
			state.agreementModalShown = true
		},*/
		agreementModalShow () {
			window.open('https://aircheckin.ru/admin-rules.docx', '_blank');
		},
		updateErrors (state, value) {
			state.errors = value
		},
		setLoading (state, value = true) {
			state.processing = value
		},
	},

	actions: {
		register ({ commit }, userData) {
			commit('setLoading')

			let url = 'register'
			let isInvite = false

			if (userData.userIdHash) {
				url = 'register-by-invite'
				isInvite = true
			}

			axios.post(url, userData)
				.then(({ data: { user, token } }) => {
					commit('auth/SET_USER', user, { root: true })
					commit('auth/SET_TOKEN', token, { root: true })

					if (isInvite) {
						router.push('/')
					} else {
						router.push({ name: 'verification', query: { email: userData.email } })
					}
				})
				.catch((error) => {

					if (error.isProcessable && error.response) {
						const { data: { errors } } = error.response
						commit('updateErrors', errors)
					}
				})
				.finally(() => {
					commit('setLoading', false)
				})
		},
		getInvitedUserData ({ dispatch }, hash) {
			return new Promise((resolve) => {
				dispatch('global/setLoading', true, { root: true })

				axios.get('invited-user-by-hash', { params: { userIdHash: hash } })
					.then(({ data: { user } }) => {
						resolve(user)
					})
					.catch(() => {
						router.push('/login')
					})
					.finally(() => {
						dispatch('global/setLoading', false, { root: true })
					})
			})

		},
	},
}
