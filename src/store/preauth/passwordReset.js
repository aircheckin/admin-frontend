import axios from '@/plugins/axios'
import { getField, updateField } from 'vuex-map-fields'
import Vue from 'vue'

export default {
	namespaced: true,

	state: {
		requestModal: false,
		requestLoading: false,
		resetModal: false,
		resetModalLoading: false,
		loading: false,
	},

	mutations: {
		setRequestLoading (state, value) {
			state.requestLoading = value
		},
		setResetLoading (state, value) {
			state.resetLoading = value
		},
		setRequestModal (state, value) {
			state.requestModal = value
		},
		setResetModal (state, value) {
			state.resetModal = value
		},
		updateField,
	},

	getters: {
		getField,
	},

	actions: {
		sendPasswordReset (context, params) {
			context.commit('setRequestLoading', true)

			axios.post('send-password-reset', params)
				.then(({ data: { message } }) => {
					Vue.prototype.$snotify.success(message)
					context.commit('setRequestModal', false)
				})
				.finally(() => {
					context.commit('setRequestLoading', false)
					context.commit('setRequestModal', false)
				})
		},

		resetPassword (context, params) {
			context.commit('setResetLoading', true)

			axios.post('reset-password', params)
				.then(({ data: { message } }) => {
					Vue.prototype.$snotify.success(message)
				})
				.finally(() => {
					context.commit('setResetModal', false)
					context.commit('setResetLoading', false)
				})
		},
	},


}
