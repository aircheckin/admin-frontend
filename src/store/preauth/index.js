import registration from '@/store/preauth/registration'
import verification from '@/store/preauth/verification'
import passwordReset from '@/store/preauth/passwordReset'

export default {
	registration,
	verification,
	passwordReset,
}
