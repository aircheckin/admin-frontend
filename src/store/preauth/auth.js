import axios from '@/plugins/axios'
import { getField, updateField } from 'vuex-map-fields'
import router from '@/router'

export default {
	namespaced: true,

	state: {
		token: localStorage.getItem('token') ?? '',
		user: {},
		loggedIn: false,
		verified: false,
		loginLoading: false,
	},

	mutations: {
		setLoginLoading (state, value) {
			state.loginLoading = value
		},

		saveToken (state, token) {
			localStorage.setItem('token', token)
			state.token = token
		},

		setAuthData (state, user) {
			state.user = user

			if (user.email_verified_at) {
				state.verified = true
			}

			state.loggedIn = true
		},

		setLoggedIn (state, value) {
			state.loggedIn = value
		},
		setVerified (state, value) {
			state.verified = value
		},

		logout (state) {
			state.loggedIn = false
			state.verified = false
			state.user = {}
			state.token = ''
			localStorage.removeItem('token')
		},
		updateField,
	},

	getters: {
		hasRole: state => roles => {
			if (state.loggedIn) {

				if (typeof roles === 'string') {
					roles = roles.split(',')
				}

				return roles.some(r => state.user.roles.indexOf(r) >= 0)
			}

			return false
		},

		hasPermission: state => permission => {
			return state.loggedIn && state.user.permissions.indexOf(permission) >= 0
		},
		getField,
	},

	actions: {
		login (context, params) {
			context.commit('setLoginLoading', true)

			axios.post('login', params)
				.then(({ data: { token, user } }) => {
					context.commit('setAuthData', user)
					context.commit('saveToken', token)
					router.push('/')
				})
				.finally(() => {
					context.commit('setLoginLoading', false)
				})
		},

		getInitData (context) {
			return new Promise((resolve, reject) => {
				axios.get('init-data')
					.then(({ data: { user, owners } }) => {
						context.commit('setAuthData', user)
						resolve(user)
					})
					.catch(() => {
						reject()
					})
			})
		},

		logout (context) {
			context.commit('logout')
			router.push('login')
		},
	},


}
