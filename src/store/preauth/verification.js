import axios from '@/plugins/axios'
import store from '@/store'
import i18n from '@/i18n'
import { getField, updateField } from 'vuex-map-fields'
import router from '@/router'
import Vue from 'vue'

export default {
	namespaced: true,

	state: {
		verificationLoading: false,
		sendLoading: false,
		sendDialog: false,
	},

	mutations: {
		updateField,
		setSendLoading (state, value) {
			state.sendLoading = value
		},
		setVerificationLoading (state, value) {
			state.verificationLoading = value
		},
		closeSendDialog (state) {
			state.sendDialog = false
		},
	},

	actions: {
		sendVerification (context, email) {
			context.commit('setSendLoading', true)

			axios.post('send-verification', { email })
				.then(() => {
					Vue.prototype.$snotify.success(i18n.tc('auth.emailSentAgain'))
				})
				.finally(() => {
					context.commit('setSendLoading', false)
					context.commit('closeSendDialog')
				})
		},
		verify (context, params) {
			context.commit('setVerificationLoading', true)

			axios.get('verify', { params })
				.then(() => {
					store.commit('auth/setVerified', true)

					if (store.state.auth.token) {
						router.push('/')
					} else {
						router.push('/login')
						Vue.prototype.$snotify.success(i18n.tc('auth.verified'))
					}
				})
				.finally(() => {
					context.commit('setVerificationLoading', false)
				})
		},
	},
	getters: {
		getField,
	},

}
