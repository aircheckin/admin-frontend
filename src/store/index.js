import Vue from 'vue'
import Vuex from 'vuex'
import preauth from '@/store/preauth'
import auth from '@/store/auth'
import global from '@/store/global/index.js'
import users from './users'
import owners from './owners'
import apartments from './apartments'
import owners_contracts from './owners_contracts'
import bookings from './bookings'
import renters from './renters'

Vue.use(Vuex)

export default new Vuex.Store({
	modules: {
		...preauth,
		auth,
		global,

		users,
		owners,
		apartments,
		owners_contracts,
		bookings,
		renters,
	},
})
