import * as crud from '@/utils/crud'
import {
	SET_ERRORS,
	SET_FORM_LOADING,
	SET_ITEM_LOADING,
	SET_ITEM_VALUES,
	SET_LOADING,
	UPDATE_ITEM,
} from './mutation_types'
import { url } from './index'
import axios from '@/plugins/axios'
import Vue from 'vue'
import i18n from '@/i18n'
import clone from 'lodash/clone'


export const getItem = ({ dispatch, commit }, id) =>
	crud.getItem({ dispatch, commit }, url, id)

export const getItems = ({ dispatch, commit }, query) =>
	crud.getItems({ dispatch, commit }, url, query)

export const create = ({ dispatch, commit, state }) =>
	crud.create({ dispatch, commit, state }, url)

export const update = ({ dispatch, commit, state }) =>
	crud.update({ dispatch, commit, state }, url)

export const setLoading = ({ commit }, value) => commit(SET_LOADING, value)

export const setItemLoading = ({ commit }, value) => commit(SET_ITEM_LOADING, value)

export const remove = ({ dispatch, commit }, item) =>
	crud.remove({ dispatch, commit }, url, item)

export const reset = ({ commit }) => crud.reset({ commit })

export const resetErrors = ({ commit }) => commit(SET_ERRORS, {})

export const getAddressCoords = ({ commit }, address) => {
	commit(SET_FORM_LOADING, true)

	return axios.get('locations/address-coords', { params: { address } })
		.then(({ data: { lat, lon } }) => {
			/*
									commit('updateField', {path: 'item.lat', value: lat})
									commit('updateField', {path: 'item.lon', value: lon})*/

			commit(SET_ITEM_VALUES, { lat, lon })
		})
		.catch(() => {
			commit(SET_ERRORS, { address: ' ' })
		})
		.finally(() => {
			commit(SET_FORM_LOADING, false)
		})
}

export const getCoordsAddress = ({ commit }, { lat, lon }) => {
	commit(SET_FORM_LOADING, true)

	return axios.get('locations/coords-address', { params: { lat, lon } })
		.then(({ data: { city, street, house } }) => {
			const address = `${city}, ${street}, ${house}`
			commit(UPDATE_ITEM, { address, lat, lon })
		})
		.finally(() => {
			commit(SET_FORM_LOADING, false)
		})
}

export const uploadImage = (_, file) => {
	let formData = new FormData()
	formData.append('file', file)

	return new Promise((resolve, reject) => {
		axios.post('apartments/images', formData)
			.then(({ data: { id } }) => {
				resolve(id)
			})
			.catch(() => {
				reject()
			})
	})


}

export const deleteImage = (_, id) => {

	return axios.delete('apartments/images/' + id)
}

export const createApartment = ({ commit, state }) => {
	let fields = clone(state.item)
	fields.images = fields.images.map(image => image.id)

	return new Promise((resolve, reject) => {
		axios.post('apartments', fields)
			.then(() => {
				resolve()
				Vue.prototype.$snotify.success(i18n.tc('apartments.created'))
			})
			.catch(({ response }) => {
				const { data: { errors } } = response

				if (errors) {
					commit(SET_ERRORS, errors)
				}

				reject()
			}).finally(() => {

		})
	})
}

export const updateApartment = ({ commit, state }) => {
	let fields = clone(state.item)
	fields.images = fields.images.map(image => image.id)

	return new Promise((resolve, reject) => {
		axios.put('apartments/' + fields.id, fields)
			.then(({ data: { data } }) => {
				commit(SET_ITEM_VALUES, data)
				resolve()
				Vue.prototype.$snotify.success(i18n.tc('apartments.updated'))
			})
			.catch(() => {
				reject()
			}).finally(() => {
		})
	})
}


export const getApartments = (_, query = {}) => {
	return axios.get(url, { params: query })
}
