import * as types from './mutation_types'
import defaultState from './state'
import { updateField } from 'vuex-map-fields'

export default {
	updateField,
	[types.SET_ITEM] (state, item) {
		Object.assign(state, { item })
	},
	[types.UPDATE_ITEM] (state, item) {
		state.item = Object.assign({}, state.item, item)
	},
	[types.SET_ITEM_LOADING] (state, value) {
		state.itemLoading = value
	},
	[types.SET_ITEMS] (state, items) {
		Object.assign(state, { items })
	},
	[types.SET_ITEM_VALUES] (state, values) {
		Object.assign(state.item, values)
	},
	[types.SET_LOADING] (state, value) {
		state.loading = value
	},
	[types.SET_ERROR] (state, error) {
		Object.assign(state, { error })
	},
	[types.SET_ERRORS] (state, errors) {
		Object.assign(state, { errors })
	},
	[types.SET_FORM_LOADING] (state, value) {
		state.formLoading = value
	},
	[types.RESET] (state) {
		Object.assign(state, defaultState())
	},
}
