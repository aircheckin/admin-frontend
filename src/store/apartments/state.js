export default function () {
	return {
		editDialog: false,
		formLoading: false,

		itemLoading: false,
		item: {
			address: null,
			lat: 55.751016,
			lon: 37.618898,
			description: null,
			comfort: [],
			name: null,
			wifi_name: null,
			wifi_password: null,
			entrance: null,
			floor: null,
			number: null,
			guests: 1,
			price: null,
			square: null,
			images: [],
		},

		items: [],
		loading: false,

		error: null,
		errors: {},
	}
}
