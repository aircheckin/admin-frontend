export default function () {
	return {
		loading: false,
		leftMenuShown: false,
		currentItemId: null,
		bookingSources: {},
		paymentMethods: {},
	}
}
