import { SET_LOADING } from './mutation_types'

export const setLoading = ({ commit }, value) => commit(SET_LOADING, value)
