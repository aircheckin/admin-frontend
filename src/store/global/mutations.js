import * as types from './mutation_types'
import { updateField } from 'vuex-map-fields'

export default {
	updateField,
	[types.SET_LOADING] (state, value) {
		state.loading = value
	},

	[types.SET] (state, value) {
		Object.assign(state, value)
	},
}
